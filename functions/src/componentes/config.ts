const functions = require( 'firebase-functions')
const firebase = require( 'firebase-admin')


firebase.initializeApp(functions.config().firebase)
const db = firebase.firestore()
const Colección_de_usuarios = db.collection('Usuarios')

export {
  firebase,
  functions,
  db,
  Colección_de_usuarios,
}
