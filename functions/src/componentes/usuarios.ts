import {firebase, functions, Colección_de_usuarios} from './config';

exports.ascender_a = functions.https.onCall( ({uid, correo_electronico, rol}:any, context:any) => {
  return ascender_a(uid, rol)
  .then(() => ({mensaje: `Se ha modificado los privilegios de ${correo_electronico}.`}) )
  .catch( error => ({mensaje: error}) )
})

async function ascender_a(uid: any, rol: any) {
  const user = await firebase.auth().getUser(uid)
  const valor = (user.customClaims && user.customClaims[rol] === true) ? false : true
  let claims = user.customClaims ? user.customClaims : {}
  claims = {...claims, [rol]: valor }
  Colección_de_usuarios.doc(user.uid).update({claims})
  return firebase.auth().setCustomUserClaims(user.uid, claims)
}