import firebase from "@firebase/app"
require("firebase/firestore")
require('firebase/auth')
require('firebase/storage')
require("firebase/functions")

let config   = {
  apiKey: "AIzaSyADB7FKWyf91atjp1yJAlF7R4yftSwbciI",
  authDomain: "gimansio12.firebaseapp.com",
  databaseURL: "https://gimansio12.firebaseio.com",
  projectId: "gimansio12",
  storageBucket: "gimansio12.appspot.com",
  messagingSenderId: "331955205770",
  appId: "1:331955205770:web:fba4bde8dd325cff81df73",
  measurementId: "G-GNW1NY3FN9"
}
 
firebase.initializeApp(config)

const db = firebase.firestore()
const auth = firebase.auth()
const _auth = firebase.auth
const functions = firebase.app().functions('us-central1')
const _firestore = firebase.firestore
const storage = firebase.storage()
const ColeccionLanding = db.collection('Landing').doc('Contenido')
const ColeccionLandingImagenes = db.collection('Landing').doc('Imágenes')

const enviar_correo_electronico = functions.httpsCallable('correo_electronico-enviar')

export {
  db,
  functions,
  auth,
  _auth,
  storage,
  firebase,
  ColeccionLanding,
  _firestore,
  ColeccionLandingImagenes,
  enviar_correo_electronico
}
