import moment from 'moment'
import 'moment/locale/es'
moment.locale('es');


const addDay = second=>moment(second).add(1, 'days').format('YYYY-MM-DD') 

const cadena_fecha_actual= fecha => {
  let partes = fecha.split('-')
  return new Date(partes[0], partes[1]-1, partes[2])
}
const secondsToFullTimestamp = second => moment(second,'X').format('DD/MM/YYYY hh:mm')
const imprimir_fecha = second => moment(second,'X').format('DD/MM/YYYY')
const secondsToFormatDateInput = second => moment(second,'X').format('YYYY-MM-DD')
const fecha_de_hoy = ()=>moment().format('YYYY-MM-DD')
const timeStampString = () => moment().format('YYYYMMDDhhmmss')
const formato_para_guardar = second => moment(second,'X').toDate()

const capitalize = val => val.charAt(0).toUpperCase() + val.slice(1)

const dias_de_la_semana = [
      {valor: 1, texto: capitalize(moment(1, 'd').format('dddd'))},
      {valor: 2, texto: capitalize(moment(2, 'd').format('dddd'))},
      {valor: 3, texto: capitalize(moment(3, 'd').format('dddd'))},
      {valor: 4, texto: capitalize(moment(4, 'd').format('dddd'))},
      {valor: 5, texto: capitalize(moment(5, 'd').format('dddd'))},
      {valor: 6, texto: capitalize(moment(6, 'd').format('dddd'))},
      {valor: 7, texto: capitalize(moment(0, 'd').format('dddd'))}
]
const obtener_periodo_con_fecha = fecha=>moment(fecha).format('YYYYMM').toUpperCase()
const obtener_dia_con_fecha = fecha=>moment(fecha).format('DD').toUpperCase()

const obtener_periodo = () => moment(new Date()).format('YYYYMM').toUpperCase()
const obtener_dia= ()=>moment(new Date()).format('DD').toUpperCase()
const obtener_hora = ()=>moment(new Date()).format('hh:mm')

const fecha_a_hora = fecha => moment(fecha,'X').format('hh:mm')

const cleanObject = object=> Object.keys(object).map(item=>object[item])
const dia_de_ayer = moment().subtract(1, 'days').startOf('day')
const dia_de_hoy = moment().subtract(1, 'days').startOf('day')
const toSlug = str=> {
    str = str.replace(/^\s+|\s+$/g, ''); // trim
    str = str.toLowerCase();

    // remove accents, swap ñ for n, etc
    var from = "ãàáäâẽèéëêìíïîõòóöôùúüûñç·/_,:;";
    var to   = "aaaaaeeeeeiiiiooooouuuunc------";
    for (var i=0, l=from.length ; i<l ; i++) {
        str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
    }

    str = str.replace(/[^a-z0-9 -]/g, '') // remove invalid chars
        .replace(/\s+/g, '-') // collapse whitespace and replace by -
        .replace(/-+/g, '-'); // collapse dashes

    return str
  }


  const group_by = (items, key) => items.reduce(
    (result, item) => ({
      ...result,
      [item[key]]: [
        ...(result[item[key]] || []),
        item,
      ],
    }), 
    {},
  );
export {
    imprimir_fecha,
    cleanObject,
    secondsToFullTimestamp,
    secondsToFormatDateInput,
    toSlug,
    timeStampString,
    obtener_periodo,
    obtener_dia,
    dia_de_ayer,
    dia_de_hoy,
    group_by,
    obtener_dia_con_fecha,
    obtener_periodo_con_fecha,
    addDay, 
    cadena_fecha_actual,
    formato_para_guardar,
    fecha_de_hoy,
    obtener_hora,
    fecha_a_hora,
    dias_de_la_semana,
}