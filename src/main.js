import Vue from 'vue'
import vuetify from './plugins/vuetify.js'
import App from './App.vue'
import router from './router'
import store from './store/store'
import './registerServiceWorker'
import {mapGetters, mapState} from 'vuex'
Vue.config.productionTip = false
let app = null

Vue.mixin({
	data: ()=>({
		requerido: v => !!v || 'Este campo es requerido',
		min_8: value => value && value.length >= 8 || 'Mínimo 8 caracteres',
		tam_9: value => value && value.length==9 || 'Se necesitan 9 caracteres',
		correo_electronico: v => /.+@.+\..+/.test(v) || 'El correo debe ser válido.',
		tam_11: value => value && value.length==11 || 'Se necesitan 11 caracteres',
		max_140: value => value && value.length<=140|| 'Máximo 140 caracteres',

	}),
	methods:{
		separar_arreglo(arreglo, cantidad) {
			const grupo = []
			let separación = []
			arreglo.map( (item, index) => {
				separación.push( item ) 
				if(separación.length == cantidad || index == arreglo.length-1) {
					grupo.push(separación)
					separación = []
				} 
			})
			return grupo
		}
	},
	computed:{
		...mapGetters({
			Contenido: 'Landing_set/contenido'
		}),
		...mapState({
			cargando: state => state.Landing_set.cargando,
			Ayudas : state => state.Ayudas
		}),
		inicio(){
			return this.$store.getters['Landing_set/contenido_especifico']({pagina: 'inicio'})
		},
		
	}
})

if(!app){
	app = new Vue({
		router,
		store,
		vuetify,
		render: h => h(App)
	}).$mount('#app')
}