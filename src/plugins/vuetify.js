import Vue from 'vue'
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'
import es from 'vuetify/es5/locale/es'

Vue.use(Vuetify)

Vue.component('my-component', {
  methods: {
    changeLocale () {
      this.$vuetify.lang.current = 'es'
    },
  },
})
export default new Vuetify({
  icons: {
      iconfont: 'fa',
  },
  lang: {
      locales: { es },
      current: 'es'
  },
  theme: {
    themes: {
      light: {
        primary:'#d5a670',
        secondary: '#e2b26d',
        verde:'#00b385',
        azul:'#39009d',
        morado:'#7d3bad',
        celeste:'#008fd2',
        negro:'#666666',
        blanco:'#FFFFFF',
        grey: '#BDBDBD',

      },
    },
  }
})