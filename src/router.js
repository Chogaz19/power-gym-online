import Vue from 'vue'
import Router from 'vue-router'
import goTo from 'vuetify/es5/services/goto'

Vue.use( Router )
const router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  scrollBehavior: (to, from, savedPosition) => {
    let scrollTo = 0
    if (to.hash)  scrollTo = to.hash
    else if (savedPosition)  scrollTo = savedPosition.y
    console.log (scrollTo)
    
    return goTo(scrollTo)
  },
  routes: [
    {
      path: '/',
      name: 'Inicio',
      component: () => import('./app/Inicio/index'),
    },

  ]
})

export default router 